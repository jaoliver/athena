

#include "../TrigCaloClusterMakerMT.h"
#include "../TrigCaloTowerMakerMT.h"
#include "../TrigCaloClusterCalibratorMT.h"

#include "../HLTCaloCellMaker.h"
#include "../HLTCaloCellSumMaker.h"
#include "../HLTCaloCellCorrector.h"

DECLARE_COMPONENT( TrigCaloTowerMakerMT )
DECLARE_COMPONENT( TrigCaloClusterCalibratorMT )
DECLARE_COMPONENT( TrigCaloClusterMakerMT )
DECLARE_COMPONENT( HLTCaloCellMaker )
DECLARE_COMPONENT( HLTCaloCellSumMaker )
DECLARE_COMPONENT( HLTCaloCellCorrector )

